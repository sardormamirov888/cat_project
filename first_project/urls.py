
from django.conf import settings
from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='CAT')

app_name='basic_app'


urlpatterns = [
    path('swagger/', schema_view),
    path('admin/', admin.site.urls),
    path('', include('basic_app.urls'))
]

urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns +=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns +=static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)

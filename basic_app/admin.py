from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(models.Kurslar)
admin.site.register(models.Kurs_bosqichi)
admin.site.register(models.Mutahasislik)
admin.site.register(models.Oqituvchilar)
admin.site.register(models.Oquvchi)
admin.site.register(models.Direktor)

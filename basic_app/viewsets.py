from .import models, serializers
from rest_framework.response import Response
from rest_framework import generics, mixins, viewsets
from rest_framework.decorators import action



class KurslarViewset(viewsets.ModelViewSet):
    serializer_class= serializers.KurslarSerializer
    queryset=models.Kurslar.objects.all()
    
    @action(methods=['put', 'post', 'put', 'delete', 'patch'], detail=True, url_path='kurslar', url_name='url_name')
    def my_method(self, request, *args, **kwargs):
        return Response ({'succes':True, 'message':'MyMethodga murojat qilindi'})
    
class DirektorViewset(viewsets.ModelViewSet):
    serializer_class= serializers.DirektorSerializer
    queryset=models.Direktor.objects.all()
    
    @action(methods=['put', 'post', 'put', 'delete', 'patch'], detail=True, url_path='direktor', url_name='url_name')
    def my_method(self, request, *args, **kwargs):
        return Response ({'succes':True, 'message':'MyMethodga murojat qilindi'})

from rest_framework import serializers
from .import models

class Kurs_bosqichiSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Kurs_bosqichi
        fields='__all__'
        
class MutahasislikSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Mutahasislik
        fields='__all__'
        
class OqituvchiSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Oqituvchilar
        fields='__all__'
        
class KurslarSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Kurslar
        fields='__all__'
        
class OquvchiSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Oquvchi
        fields='__all__'
        
class DirektorSerializer(serializers.ModelSerializer):
    class Meta:
        model=models.Direktor
        fields='__all__'
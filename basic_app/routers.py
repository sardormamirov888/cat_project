from rest_framework.routers import DefaultRouter
from .import viewsets

router= DefaultRouter()
router.register(r'kurs',viewsets.KurslarViewset, basename='kurs')
router.register(r'direktor',viewsets.DirektorViewset, basename='direktor')


urlpatterns=router.urls

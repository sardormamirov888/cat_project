from django.urls import path, include
from .import views
app_name='basic_app'
urlpatterns = [
    path('oqituvchi/', views.OqituvchiApi.as_view()),
    path('oqituvchi/<int:pk>', views.OqituvchiCreateApi.as_view()),
    
    path('oquvchi/', views.OquvchiApi.as_view()),
    path('oquvchi/<int:pk>', views.OquvchiCreateApi.as_view()),
    
    path('', views.MutahasislikMixin.as_view()),
    
    path('kurs-bosqichi/', views.Kurs_bosqichiMixin.as_view()),
    
    path('viewset/', include('basic_app.routers')),
    
]

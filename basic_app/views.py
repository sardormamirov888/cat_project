from django.shortcuts import render
from .import models
from .import serializers
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins

# Create your views here.

class OqituvchiApi(generics.ListCreateAPIView):
    queryset=models.Oqituvchilar.objects.all()
    serializer_class=serializers.OqituvchiSerializer
    
    filter_backends=[DjangoFilterBackend]
    filterset_fields=['l_name', 'f_name']
    
    
class OqituvchiCreateApi(generics.RetrieveUpdateDestroyAPIView):
    queryset=models.Oqituvchilar.objects.all()
    serializer_class=serializers.OqituvchiSerializer
    
    
    

class OquvchiApi(generics.ListCreateAPIView):
    queryset=models.Oquvchi.objects.all()
    serializer_class=serializers.OquvchiSerializer

class OquvchiCreateApi(generics.RetrieveUpdateDestroyAPIView):
    queryset=models.Oquvchi.objects.all()
    serializer_class=serializers.OquvchiSerializer


class MutahasislikMixin(generics.GenericAPIView, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                         mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = serializers.MutahasislikSerializer
    queryset = models.Mutahasislik.objects.all()
    lookup_field='id'

    
    def get(self, request,  *args, **kwargs):
        if kwargs.get('id'):
            return self.retrieve(request,  *args, **kwargs)
        return self.list(request,  *args, **kwargs)
    
    def post(self,request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
       
    def put(self,request,*args, **kwargs):
        return self.update(request,*args, **kwargs)
    
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)
    
    def delete(self,request,*args, **kwargs):
        return self.destroy(request,*args, **kwargs)


class Kurs_bosqichiMixin(generics.GenericAPIView, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                         mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = serializers.Kurs_bosqichiSerializer
    queryset = models.Kurs_bosqichi.objects.all()
    lookup_field='id'

    
    def get(self, request,  *args, **kwargs):
        if kwargs.get('id'):
            return self.retrieve(request,  *args, **kwargs)
        return self.list(request,  *args, **kwargs)
    
    def post(self,request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
       
    def put(self,request,*args, **kwargs):
        return self.update(request,*args, **kwargs)
    
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)
    
    def delete(self,request,*args, **kwargs):
        return self.destroy(request,*args, **kwargs)
    
    
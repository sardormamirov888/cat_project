from django.db import models

# Create your models here.
class Kurs_bosqichi(models.Model):
    k_level=models.CharField(max_length=30)
    s_level=models.CharField(max_length=30)
    
    def __str__(self):
        return self.k_level
    
    
class Mutahasislik(models.Model):
    mutahasis=models.CharField(max_length=35)
    def __str__(self):
        return self.mutahasis
    
    

class Kurslar(models.Model):
    kurs=models.CharField(max_length=50, verbose_name='kurslar')
    def __str__(self):
        return self.kurs
    
    
class Oqituvchilar(models.Model):
    f_name=models.CharField(max_length=30, verbose_name='first_name')
    l_name=models.CharField(max_length=30, verbose_name='last_name')
    mutahasislik_id=models.ForeignKey(Mutahasislik, on_delete=models.CASCADE)
    kurs_id=models.ForeignKey(Kurslar, on_delete=models.CASCADE)
    def __str__(self):
            return self.f_name

class Oquvchi(models.Model):
    f_name=models.CharField(max_length=30, verbose_name='first_name')
    l_name=models.CharField(max_length=30, verbose_name='last_name')
    email=models.EmailField(unique=True)
    kurs_bosqichi_id=models.ForeignKey(Kurs_bosqichi, on_delete=models.CASCADE)
    kurs_id=models.ForeignKey(Kurslar, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.f_name
    
class Direktor(models.Model):
    oqituvchi_id=models.ForeignKey(Oqituvchilar, on_delete=models.CASCADE)
    oquvchi_id=models.ForeignKey(Oquvchi, on_delete=models.CASCADE)
    direktor=models.CharField(max_length=50, verbose_name='full name')
    
    def __str__(self):
        return self.direktor


    

   
